import RPi.GPIO as GPIO
from gpiozero import LED
import time
import signal
import sys

binCapacity = "Empty"

# use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BCM)

# set GPIO Pins
led = LED(17)
pinTrigger = 18
pinEcho = 24

#Turn on the LED for two seconds
led.on()
time.sleep(2)
led.off()

# set GPIO input and output channels
GPIO.setup(pinTrigger, GPIO.OUT)
GPIO.setup(pinEcho, GPIO.IN)

while True:
        # set Trigger to HIGH
        GPIO.output(pinTrigger, True)
        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(pinTrigger, False)

        startTime = time.time()
        stopTime = time.time()

        # save start time
        while 0 == GPIO.input(pinEcho):
                startTime = time.time()

        # save time of arrival
        while 1 == GPIO.input(pinEcho):
                stopTime = time.time()

        # time difference between start and arrival
        TimeElapsed = stopTime - startTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2

        print ("Distance: %.1f cm" % distance)
        time.sleep(5)
        

        if distance <= 20 and distance > 0:
          if binCapacity == "Empty":
            binCapacity = "Full"
            print ("Time to to replace the bin")
            led.on()
            
        elif distance > 20:
          if binCapacity == "Full":
            binCapacity = "Empty"
            print("Thanks for emptying the bin")
            led.off()
        
        else:
          led.off()